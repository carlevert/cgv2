/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Object.cpp
 *
 *  Contains vertex and normals data. Contains a method that can calculate
 *  normals from faces and a method that squeezes the model in a unit cube.
 *
 */

#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include <vector>
#include <iostream>
#include <cstring>
#include <cmath>
#include <iomanip>

#include "Face.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;

class Object {

public:
  Object();
  ~Object();

  unsigned int GetNumVertices();
  void SetNumVertices(unsigned num_vertices);

  unsigned int GetNumFaces();
  void SetNumFaces(int num_faces);
  
  float* GetVertices();
  void AddVertex(float* vertex);

  float* GetNormals();

  Face* GetFace(unsigned int i);
  void AddFace(Face* face);

  void CalculateNormals();
  void Normalize();
  void Triangulate();

  void Print();

private:

  unsigned int num_vertices = 0;
  unsigned int num_faces = 0;
  unsigned int num_edges = 0;

  unsigned int vertex_cursor = 0;
  vec3* vertices = nullptr;
  vec3* normals = nullptr;

  vector<Face*> faces;

};

#endif
