#ifndef _GUICALLBACK_H_
#define _GUICALLBACK_H_

#include <gtk/gtk.h>
#include <glib.h>
#include <stdlib.h>
#include <math.h>

#include "guicontrol.h"

// --------------------

extern void* app;
struct Application;

// Misc

void call_OpenOffFile(struct Application* a, char* filename);
void call_FlipNormals(struct Application* a, int flip_normals);

// Light

void call_Light_SetPositionX(struct Application* a, float value);
void call_Light_SetPositionY(struct Application* a, float value);
void call_Light_SetPositionZ(struct Application* a, float value);

void call_Light_SetIntensityR(struct Application* a, float value);
void call_Light_SetIntensityG(struct Application* a, float value);
void call_Light_SetIntensityB(struct Application* a, float value);

// Ambient light

void call_Ambient_SetIntensityR(struct Application* a, float value);
void call_Ambient_SetIntensityG(struct Application* a, float value);
void call_Ambient_SetIntensityB(struct Application* a, float value);

// Material

void call_Material_SetAmbientCoefficientR(struct Application* a, float value);
void call_Material_SetAmbientCoefficientG(struct Application* a, float value);
void call_Material_SetAmbientCoefficientB(struct Application* a, float value);

void call_Material_SetSpecularCoefficientR(struct Application* a, float value);
void call_Material_SetSpecularCoefficientG(struct Application* a, float value);
void call_Material_SetSpecularCoefficientB(struct Application* a, float value);

void call_Material_SetDiffuseCoefficientR(struct Application* a, float value);
void call_Material_SetDiffuseCoefficientG(struct Application* a, float value);
void call_Material_SetDiffuseCoefficientB(struct Application* a, float value);

void call_SetPhongValue(struct Application* a, int value);

// Perspective

void call_SetFov(struct Application*, float fov);
void call_SetParallellProjection(struct Application*);
void call_SetPerspectiveProjection(struct Application*);
void call_SetTop(struct Application*, float top);
void call_SetFar(struct Application*, float far);
void call_SetObliqueScale(struct Application*, float oblique_scale);
void call_SetObliqueAngle(struct Application*, float oblique_angle);

void call_SetTextureFilename(struct Application*, char* filename);
void call_ToggleTexture(struct Application*, int show_texture);

// --------------------

/* CALLBACKS FROM GUI */

void     on_off_chooser_selection_changed(GtkFileChooser*, gpointer);
void     on_flip_normals_toggled(GtkToggleButton*, gpointer);
void     on_light_x_value_changed(GtkAdjustment*, gpointer);
void     on_light_y_value_changed(GtkAdjustment*, gpointer);
void     on_light_z_value_changed(GtkAdjustment*, gpointer);
void     on_I_light_r_value_changed(GtkAdjustment*,gpointer);
void     on_I_light_g_value_changed(GtkAdjustment*, gpointer);
void     on_I_light_b_value_changed(GtkAdjustment*, gpointer);
void     on_I_ambient_r_value_changed(GtkAdjustment*, gpointer);
void     on_I_ambient_g_value_changed(GtkAdjustment*, gpointer);
void     on_I_ambient_b_value_changed(GtkAdjustment*, gpointer);
void     on_k_ambient_r_value_changed(GtkAdjustment*, gpointer);
void     on_k_ambient_g_value_changed(GtkAdjustment*, gpointer);
void     on_k_ambient_b_value_changed(GtkAdjustment*, gpointer);
void     on_k_diffuse_r_value_changed(GtkAdjustment*, gpointer);
void     on_k_diffuse_g_value_changed(GtkAdjustment*, gpointer);
void     on_k_diffuse_b_value_changed(GtkAdjustment*, gpointer);
void     on_k_specular_r_value_changed(GtkAdjustment*, gpointer);
void     on_k_specular_g_value_changed(GtkAdjustment*, gpointer);
void     on_k_specular_b_value_changed(GtkAdjustment*, gpointer);
void     on_phong_value_changed(GtkAdjustment*, gpointer);
void     on_top_value_changed(GtkAdjustment*, gpointer);
void     on_far_value_changed(GtkAdjustment*, gpointer);
void     on_oblique_scale_value_changed(GtkAdjustment*, gpointer);
void     on_oblique_angle_value_changed(GtkAdjustment*, gpointer);
void     on_fov_value_changed(GtkAdjustment*, gpointer);
void     on_parallel_toggled(GtkToggleButton*, gpointer);
void     on_perspective_toggled(GtkToggleButton*, gpointer);
void     on_texture_chooser_selection_changed(GtkFileChooser*, gpointer);
void     on_texture_show_toggled(GtkToggleButton*, gpointer);
void     on_win_destroy_event();

#endif
