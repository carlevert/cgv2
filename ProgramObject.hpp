/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  ProgramObject.hpp
 *
 *  Represents an OpenGL program.
 *
 */

#ifndef PROGRAMOBJECT_HPP_
#define PROGRAMOBJECT_HPP_

#include <string>
#include <fstream>
#include <cstring>
#include <iostream>
#include <GL/glew.h>
#include "Shader.hpp"

using namespace std;

class ProgramObject
{
private:
	GLuint program_id;
	ProgramObject();
	void AttachShader(Shader * shader);
	void LinkProgram();
	bool LinkedSuccessfully();
	string GetLinkingErrorMessage();
public:
	~ProgramObject();
	GLuint GetAttribLocation(const GLchar* name);
	GLuint GetUniformLocation(const GLchar* name);
	void Use();
	GLuint GetProgramId();
	static ProgramObject* CreateFromFiles(string vertex_shader, string fragment_shader);
};

#endif
