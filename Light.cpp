/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Light.cpp
 *
 *  Class Light is holding point light properties.
 *
 */

#include "Light.hpp"

Light::Light() {
	intensity = vec3(0.5f, 0.5f, 0.5f);
}

Light::~Light() {
}

/*
 * Getter method for lights position, returns pointer to floats.
 */
float* Light::GetPosition() {
	return value_ptr(position);
}

/*
 * Setter for lights X-position.
 */
void Light::SetPositionX(float value) {
	this->position.x = value;
	InvalidateLightPos();
}

/*
 * Setter for lights Y-position.
 */
void Light::SetPositionY(float value) {
	this->position.y = value;
	InvalidateLightPos();
}

/*
 * Setter for lights Z-position.
 */
void Light::SetPositionZ(float value) {
	this->position.z = value;
	InvalidateLightPos();
}

/*
 * Getter-method for light intensity. Returns intensity as
 *  pointer to floats.
 */
float* Light::GetIntensity() {
	return value_ptr(intensity);
}

/*
 * Setter for red component of light intensity.
 */
void Light::SetIntensityR(float intensity) {
	this->intensity.r = intensity;
	InvalidateLightPos();
}

/*
 * Setter for green component of light intensity.
 */
void Light::SetIntensityG(float intensity) {
	this->intensity.g = intensity;
	InvalidateLightPos();
}

/*
 * Setter for blue component of light intensity.
 */
void Light::SetIntensityB(float intensity) {
	this->intensity.b = intensity;
	InvalidateLightPos();
}

/*
 * Flag changes in light position or intensity.
 */
void Light::InvalidateLightPos() {
	if (invalid_light_pos != nullptr)
		*invalid_light_pos = true;
}

/*
 * Sets pointer to a boolean variable which indicates change in light.
 */
void Light::SetInvalidLightPosRef(bool* invalid_light_pos) {
	this->invalid_light_pos = invalid_light_pos;
}
