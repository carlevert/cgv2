/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  FragmentShader.glsl
 *
 *	Phong shading
 *	When texture is used it is multiplied with the diffuse part of the material.
 *
 */
 

#version 330 core

in vec2 UV;
out vec3 fColor;

in vec3 world_normal;
in vec3 light_direction;
in vec3 world_position;

uniform float show_texture;
uniform sampler2D myTextureSampler;

// Light source
uniform vec3 light_pos;
uniform vec3 light_intensity;

// Ambient light
uniform vec3 ambient_light_intensity;

// Material properties
uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;
uniform int phong;

// Normals
uniform float flip_normals;

uniform mat4 view;

void main() {

	vec3 normalized_world_normal = normalize(world_normal);
	vec3 light_direction = normalize(light_pos - world_position);

	// float attenuation = 1.0f / distance(light_pos, world_position);

	vec3 I_ambient = ambient * ambient_light_intensity;

	vec3 I_diffuse = diffuse * light_intensity * max(dot(light_direction, normalized_world_normal), 0);

	vec3 r = 2.0f * dot(light_direction, normalized_world_normal) * normalized_world_normal - light_direction;
	vec3 camera_position = vec3(-view[3][0], -view[3][1], -view[3][2]);
	vec3 v = camera_position - world_position;

	v = normalize(v);

	vec3 I_specular = specular * light_intensity * max(pow(dot(r, v), phong), 0.0f);

	if (dot(normalized_world_normal, v) <= 0)
		I_specular = vec3(0,0,0);


	vec3 mult_color = I_diffuse;
	vec3 additive_color = I_ambient + I_specular;



	if (show_texture > 0.5)
		fColor = mult_color * texture(myTextureSampler, UV ).rgb + additive_color;
	else
		fColor = mult_color + additive_color;

}
