/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Camera.hpp
 *
 *  Camera can be translated in camera space and rotated aorund U or V. Also
 *  responsible for creating the view-matrix.
 *
 */

#ifndef CAMERA_HPP
#define CAMERA_HPP

#define _USE_MATH_DEFINES
#include <cmath>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>

using namespace glm;

class Camera {
private:
	mat4 identity = mat4(1.0f);
	mat4 rotation_matrix_u;
	mat4 rotation_matrix_v;
	mat4 rotation_matrix_z = mat4(1.0f);
	vec4 translation = vec4(0.0f);
	mat4 translation_matrix = mat4(1.0f);
	mat4 rotation_matrix = mat4(1.0f);

	vec3 initial_position = vec3(0.0f, 0.0f, 2.0f);

	void InvalidateViewMatrix();
	bool* invalid_view_matrix = nullptr;

public:
	Camera();
	~Camera();
	void SetInvalidViewMatrixRef(bool* invalid_view_matrix);

	void Translate(vec3 translation);
	void RotateU(float rotation);
	void RotateV(float rotation);

	mat4 GetViewMatrix();

	void Commit();
};

#endif
