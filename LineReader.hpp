/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  LineReader.hpp
 *
 *  Handy class for reading one line at a time. Keeps track of current line
 *  which is useful when creating error messages in case of parse errors.
 *
 */

#ifndef LINEREADER_HPP_
#define LINEREADER_HPP_

#include <fstream>
#include <iostream>
#include <string>
#include "boost/optional.hpp"

using namespace std;

class LineReader {
public:
	static LineReader* Open(string filename);
	int GetLineNo();
	boost::optional<string> GetLine();
	string GetFilename();
	~LineReader();
private:
	LineReader();
	ifstream* input_stream;
	string filename;
	int line_no;
};

#endif
