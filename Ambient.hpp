/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Ambient.hpp
 *
 *  Class Ambient is holding the ambient light properties.
 *
 */

#ifndef AMBIENT_HPP_
#define AMBIENT_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;

class Ambient {
public:
	Ambient();
	virtual ~Ambient();
	float* GetIntensity();
	void SetIntensityR(float intensity);
	void SetIntensityG(float intensity);
	void SetIntensityB(float intensity);
	void SetInvalidateAmbientLightRef(bool* invalid_light_pos);
private:
	void InvalidateAmbientLight();
	bool* invalid_ambient_light_properties = nullptr;
	vec3 intensity;
};

#endif

