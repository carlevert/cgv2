/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  main.cpp
 *
 *  Contains main-function. Creates an OpenGL context with GLFW.
 *
 */

#include <thread>
#include <ctime>
#include <sys/stat.h>

#include "main.hpp"

int main(int argc, char** argv)
{

	GLFWwindow* window = SetupGlfw();

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		std::cout << "glewInit() failed" << std::endl;
	}

	// This application object keeps contains most of this program
	// except the GUI handed out with the assignment.
	application = new Application();

	// Pointer to the application object used by C functions.
	app = (void*) application;

	guiInit(&argc, argv);
	guiInitWindow("ass3gui.glade");

	// Sets some default values to lights etc. to the GUI which also
	// synchronizes the corresponding attributes in objects contained in
	// application-object.
	SetGuiDefaults();

	// Start one thread for eating events from the GUI.
	thread do_gui_main_iteration_thread([]() {
		while (running)
		guiMainIteration();
	});

	// Another thread for checking whether shader source has changed.
	thread reread_shader_source(
			[]() {
				time_t current_shaders_last_mod_time[2];
				current_shaders_last_mod_time[0] = 0;
				current_shaders_last_mod_time[1] = 0;
				struct stat shader_source_attrib[2];
				char vertex_shader_filename[] = "VertexShader.glsl";
				char fragment_shader_filename[] = "FragmentShader.glsl";
				char* shader_filenames[] = {vertex_shader_filename, fragment_shader_filename};

				do {
					for (int i = 0; i < 2; i++) {
						stat(shader_filenames[i], &shader_source_attrib[i]);
						if (current_shaders_last_mod_time[i] < shader_source_attrib[i].st_mtime) {
							current_shaders_last_mod_time[i] = shader_source_attrib[i].st_mtime;
							application->shader_source_changed = true;
						}
					}
					usleep(10000);
				}
				while (running);
			});

	// Yet another main-loop.
	while (running) {
		if (rotating_camera_x_y) {
			glfwGetCursorPos(window, &end_x, &end_y);
			float dx = (float) (end_x - start_x);
			float dy = (float) (end_y - start_y);
			application->GetCamera()->RotateU(dy * rotation_scale);
			application->GetCamera()->RotateV(dx * rotation_scale);
		}
		glfwPollEvents();
		// Files needs to be loaded by the main thread, so from the GUI-thread
		// only the filename of the desired OFF-file is set.
		if (application->off_to_load != 0) {
			application->OpenOffFile(application->off_to_load);
			application->off_to_load = 0;
		}
		application->Display();
		glfwSwapBuffers(window);
	}

	delete application;

	return 0;
}

/*
 * Creates a window with an OpenGL context. Callbacks for key- and mouse
 * events (among other things) are also set in this function.
 */
GLFWwindow* SetupGlfw()
{
	if (!glfwInit()) {
		std::cerr << "Could not initialize GLFW" << std::endl;
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_DEPTH_BITS, 16);
	glfwWindowHint(GLFW_SAMPLES, 8);
	GLFWwindow* window = glfwCreateWindow(Application::INITIAL_WINDOW_WIDTH,
			Application::INITIAL_WINDOW_HEIGHT, "Assignment 2", NULL, NULL);

	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSetErrorCallback(GlfwErrorCallback);
	glfwSetMouseButtonCallback(window, GlfwMouseButtonCallback);
	glfwSetKeyCallback(window, GlfwKeyCallback);
	glfwSetWindowCloseCallback(window, GlfwWindowCloseCallback);
	glfwSetFramebufferSizeCallback(window, GlfwFramebufferSizeCallback);
//	glfwSetScrollCallback(window, GlfwScrollCallback);
	glfwSwapInterval(1);

	return window;
}

/*
 * Prints an error message.
 */
void GlfwErrorCallback(int error, const char* description)
{
	cerr << error << ": " << description << endl;
}

/*
 * Stops running threads by setting global variable running to false.
 */
void GlfwWindowCloseCallback(GLFWwindow* window)
{
	running = false;
	glfwSetWindowShouldClose(window, GL_TRUE);
}

/*
 * Tells application-object that the viewport has been resized.
 */
void GlfwFramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	application->SetViewportSize(width, height);
}

/*
 * When left mouse button is pressed the user can rotate camera
 * around cameras X and Y axis.
 */
void GlfwMouseButtonCallback(GLFWwindow* window, int button, int action,
		int mods)
{
	Camera* camera = application->GetCamera();
	if (button == GLFW_MOUSE_BUTTON_LEFT) {
		if (action == GLFW_PRESS) {
			glfwGetCursorPos(window, &start_x, &start_y);
			end_x = start_x;
			end_y = start_y;
			rotating_camera_x_y = true;
		} else if (action == GLFW_RELEASE) {
			rotating_camera_x_y = false;
			glfwGetCursorPos(window, &end_x, &end_y);
			float dx = (float) (end_x - start_x);
			float dy = (float) (end_y - start_y);
			camera->Commit();
		}
	}
}

//void GlfwScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
//{
//}

void GlfwKeyCallback(GLFWwindow* window, int key, int scancode, int action,
		int mods)
{
	const float delta = 0.1f;
	const float angle = 1.0f;

	if (action == GLFW_RELEASE)
		return;

	bool has_models = application->GetSelectedModelNo() > 0;
	Model* model;
	if (has_models)
		model = application->GetModel();
	Camera* camera = application->GetCamera();
	Projection* projection = application->GetProjection();

	// Key 1 through 9 selects which model to transform
	if (key >= GLFW_KEY_1 && key <= GLFW_KEY_9)
		application->SelectModel(key - GLFW_KEY_0);

	// Transforms the currently selected model according to
	// assignment specification
	if (has_models) {
		switch (key) {
		case GLFW_KEY_LEFT:
			model->RotateY(-10.0f);
			break;
		case GLFW_KEY_RIGHT:
			model->RotateY(10.0f);
			break;
		case GLFW_KEY_UP:
			model->RotateX(-10.0f);
			break;
		case GLFW_KEY_DOWN:
			model->RotateX(10.0f);
			break;
		case GLFW_KEY_J:
			model->Translate(glm::vec2(-delta, 0.0f));
			break;
		case GLFW_KEY_L:
			model->Translate(glm::vec2(delta, 0.0f));
			break;
		case GLFW_KEY_I:
			model->Translate(glm::vec2(0.0f, delta));
			break;
		case GLFW_KEY_K:
			model->Translate(glm::vec2(0.0f, -delta));
			break;
		case GLFW_KEY_KP_ADD:
			model->Scale(1.2f);
			break;
		case GLFW_KEY_KP_SUBTRACT:
			model->Scale(1.0f / 1.2f);
			break;
		}
	}

	// Projection transformations
	switch (key) {
	case GLFW_KEY_COMMA:
		projection->SetFovDegrees(projection->GetFovDegrees() + 3.0f);
		break;
	case GLFW_KEY_PERIOD:
		projection->SetFovDegrees(projection->GetFovDegrees() - 3.0f);
		break;
	case GLFW_KEY_P:
		if (projection->GetProjectionType() == ProjectionType::PERSPECTIVE)
			projection->SetProjectionType(ProjectionType::PARALLEL);
		else
			projection->SetProjectionType(ProjectionType::PERSPECTIVE);
		break;
	case GLFW_KEY_V:
		projection->SetObliqueAngleDegrees(
				projection->GetObliqueAngleDegrees() - 5.0f);
		break;
	case GLFW_KEY_B:
		projection->SetObliqueAngleDegrees(
				projection->GetObliqueAngleDegrees() + 5.0f);
		break;
	case GLFW_KEY_N:
		projection->SetObliqueScale(projection->GetObliqueScale() - 0.1f);
		break;
	case GLFW_KEY_M:
		projection->SetObliqueScale(projection->GetObliqueScale() + 0.1f);
		break;

		// View (Camera transformations)
	case GLFW_KEY_W:
		// Up,  move p_0 and p_ref relative the cameras positive y-axis
		camera->Translate(glm::vec3(0.0f, delta, 0.0f));
		break;
	case GLFW_KEY_S:
		// Down, move p_0 and p_ref relative the cameras negative y-axis
		camera->Translate(glm::vec3(0.0f, -delta, 0.0f));
		break;
	case GLFW_KEY_A:
		// Right,  move p_0 and p_ref relative the cameras positive x-axis
		camera->Translate(glm::vec3(-delta, 0.0f, 0.0f));
		break;
	case GLFW_KEY_D:
		// Left, move p_0 and p_ref relative the cameras negative x-axis
		camera->Translate(glm::vec3(delta, 0.0f, 0.0f));
		break;
	case GLFW_KEY_Z:
		// Forward, move p_0 and p_ref relative the cameras negative z-axis
		camera->Translate(glm::vec3(0.0f, 0.0f, -delta));
		break;
	case GLFW_KEY_X:
		// Backward, move p_0 and p_ref relative the cameras positive z-axis
		camera->Translate(glm::vec3(0.0f, 0.0f, delta));
		break;

	case GLFW_KEY_Q:
		running = false;
		glfwSetWindowShouldClose(window, GL_TRUE);
		break;
	}

}

void SetGuiDefaults()
{

	bool flip_normals = false;

	double light_position[3] = { 10.0f, 10.0f, 10.0f };
	double light_intensity[3] = { 1.0, 1.0, 1.0 };
	double ambient_light_intensity[3] = { 0.0, 0.0, 0.0 };

	double material_ambient[3] = { 1.0f, 1.0f, 1.0f };
	double material_diffuse[3] = { 0.5f, 0.5f, 0.5f };
	double material_specular[3] = { 0.5f, 0.5f, 0.5f };

	double top = 1.0;
	double far = 500.0;
	double oblique_scale = 0.0;
	double oblique_angle = 45.0;

	double fov = 45.0;
	bool perspective = true;

// TODO Default texture

	gui_set_flip_normals(flip_normals);

	gui_set_light_pos(light_position);
	gui_set_I_light(light_intensity);
	gui_set_I_ambient(ambient_light_intensity);

	gui_set_k_ambient(material_ambient);
	gui_set_k_diffuse(material_diffuse);
	gui_set_k_specular(material_specular);
	gui_set_phong(1);

	gui_set_option_top(top);
	gui_set_option_far(far);
	gui_set_option_oblique_scale(oblique_scale);
	gui_set_option_oblique_angle(oblique_angle);

	gui_set_option_fov(fov);
	if (perspective)
		gui_set_projection_perspective();
	else
		gui_set_projection_parallel();

}

