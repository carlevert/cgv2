/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Shader.hpp
 *
 *  Reads shader source and compiles shader.
 *
 */

#ifndef SHADER_HPP_
#define SHADER_HPP_

#include <GL/glew.h>
#include <string>
#include <fstream>
#include <cstring>
#include <iostream>

using namespace std;

class Shader {
private:
	Shader(GLenum shader_type);
	GLenum shader_type;
	GLuint shader_id;
	GLchar* shader_source = nullptr;
	static GLchar* ReadFile(string filename);
	static streamoff GetFileLength(ifstream & file);
	void CompileShader();
	bool CompiledSuccessfully();
	string GetCompileErrorMessage();
public:
	virtual ~Shader();
	GLuint GetId();
	static Shader* CreateFromFile(string filename, GLenum shader_type);
};

class VertexShader: public Shader {
public:
	static Shader* CreateFromFile(string filename);
};

class FragmentShader: public Shader {
public:
	static Shader* CreateFromFile(string filename);
};

#endif
