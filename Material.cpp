/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Material.cpp
 *
 *  Material-objects stores information about material properties.
 *
 */

#include "Material.hpp"

Material::Material() {
}

Material::~Material() {
}

/*
 * Setter for materials ambient coefficient.
 */
void Material::SetAmbientR(float value) {
	ambient.r = value;
	InvalidateMaterial();
}

/*
 * Setter for materials ambient coefficient.
 */
void Material::SetAmbientG(float value) {
	ambient.g = value;
	InvalidateMaterial();
}

/*
 * Setter for materials ambient coefficient.
 */
void Material::SetAmbientB(float value) {
	ambient.b = value;
	InvalidateMaterial();
}

/*
 * Setter for materials diffuse coefficient.
 */
void Material::SetDiffuseR(float value) {
	diffuse.r = value;
	InvalidateMaterial();
}

/*
 * Setter for materials diffuse coefficient.
 */
void Material::SetDiffuseG(float value) {
	diffuse.g = value;
	InvalidateMaterial();
}

/*
 * Setter for materials diffuse coefficient.
 */
void Material::SetDiffuseB(float value) {
	diffuse.b = value;
	InvalidateMaterial();
}

/*
 * Setter for materials specular coefficient.
 */
void Material::SetSpecularR(float value) {
	specular.r = value;
	InvalidateMaterial();
}

/*
 * Setter for materials specular coefficient.
 */
void Material::SetSpecularG(float value) {
	specular.g = value;
	InvalidateMaterial();
}

/*
 * Setter for materials specular coefficient.
 */
void Material::SetSpecularB(float value) {
	specular.b = value;
	InvalidateMaterial();
}

/*
 * Setter for materials Phong-factor.
 */
void Material::SetPhong(unsigned int value) {
	phong = value;
	InvalidateMaterial();
}

/*
 * Setter for materials ambient coefficient.
 */
float* Material::GetAmbient() {
	return value_ptr(ambient);
}

/*
 * Getter for materials diffuse coefficient.
 */
float* Material::GetDiffuse() {
	return value_ptr(diffuse);
}

/*
 * Getter for materials specular coefficient.
 */
float* Material::GetSpecular() {
	return value_ptr(specular);
}

/*
 * Getter for materials Phong factor.
 */
unsigned int Material::GetPhong() {
	return phong;
}

/*
 * Flags material invalid.
 */
void Material::InvalidateMaterial() {
	if (invalid_material != nullptr)
		*invalid_material = true;
}

/*
 * Sets pointer to a boolean variable which indicates changes in materials
 * properties.
 */
void Material::SetInvalidMaterialRef(bool* invalid_material) {
	this->invalid_material = invalid_material;
}
