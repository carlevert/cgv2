/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  AppWrapp.cpp
 *
 *  Adapter for making C++ object accessible from C.
 *
 */

#include "AppWrap.hpp"

// Misc

void call_OpenOffFile(Application* a, char* filename) {
	a->off_to_load = filename;
}

void call_FlipNormals(Application* a, int flip_normals) {
	a->SetFlipNormals(flip_normals);
}

// Light, position

void call_Light_SetPositionX(struct Application* a, float value) {
	a->GetLight()->SetPositionX(value);
}
void call_Light_SetPositionY(struct Application* a, float value) {
	a->GetLight()->SetPositionY(value);
}
void call_Light_SetPositionZ(struct Application* a, float value) {
	a->GetLight()->SetPositionZ(value);
}

// Light, intensity

void call_Light_SetIntensityR(Application* a, float value) {
	a->GetLight()->SetIntensityR(value);
}
void call_Light_SetIntensityG(Application* a, float value) {
	a->GetLight()->SetIntensityG(value);
}
void call_Light_SetIntensityB(Application* a, float value) {
	a->GetLight()->SetIntensityB(value);
}

// Ambient

void call_Ambient_SetIntensityR(Application* a, float value) {
	a->GetAmbient()->SetIntensityR(value);
}
void call_Ambient_SetIntensityG(Application* a, float value) {
	a->GetAmbient()->SetIntensityG(value);
}
void call_Ambient_SetIntensityB(Application* a, float value) {
	a->GetAmbient()->SetIntensityB(value);
}

// Material

void call_Material_SetAmbientCoefficientR(Application* a, float value) {
	a->GetMaterial()->SetAmbientR(value);
}
void call_Material_SetAmbientCoefficientG(Application* a, float value) {
	a->GetMaterial()->SetAmbientG(value);
}
void call_Material_SetAmbientCoefficientB(Application* a, float value) {
	a->GetMaterial()->SetAmbientB(value);
}

void call_Material_SetSpecularCoefficientR(Application* a, float value) {
	a->GetMaterial()->SetSpecularR(value);
}
void call_Material_SetSpecularCoefficientG(Application* a, float value) {
	a->GetMaterial()->SetSpecularG(value);
}
void call_Material_SetSpecularCoefficientB(Application* a, float value) {
	a->GetMaterial()->SetSpecularB(value);
}

void call_Material_SetDiffuseCoefficientR(Application* a, float value) {
	a->GetMaterial()->SetDiffuseR(value);
}
void call_Material_SetDiffuseCoefficientG(Application* a, float value) {
	a->GetMaterial()->SetDiffuseG(value);
}
void call_Material_SetDiffuseCoefficientB(Application* a, float value) {
	a->GetMaterial()->SetDiffuseB(value);
}

void call_SetPhongValue(Application* a, int value) {
	a->GetMaterial()->SetPhong(value);

}

// Perspective

void call_SetFov(Application* a, float fov) {
	a->GetProjection()->SetFovDegrees(fov);
}

void call_SetParallellProjection(Application* a) {
	a->GetProjection()->SetProjectionType(ProjectionType::PARALLEL);
}

void call_SetTop(Application* a, float top) {
	a->GetProjection()->SetTop(top);
}

void call_SetFar(Application* a, float far) {
	a->GetProjection()->SetZFar(far);
}

void call_SetObliqueScale(Application* a, float oblique_scale) {
	a->GetProjection()->SetObliqueScale(oblique_scale);
}

void call_SetObliqueAngle(Application* a, float oblique_angle) {
	a->GetProjection()->SetObliqueAngleDegrees(oblique_angle);
}

void call_SetPerspectiveProjection(Application* a) {
	a->GetProjection()->SetProjectionType(ProjectionType::PERSPECTIVE);
}

void call_SetTextureFilename(Application* a, char* filename) {
	a->SetTextureFilename(filename);
	gui_set_show_texture(true);
}

void call_ToggleTexture(Application* a, int show_texture) {
	a->SetShowTexture(show_texture);
}
