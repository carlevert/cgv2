/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Model.hpp
 *
 *  Model-objects have their own vertex array and buffers, and of course a
 *  model transformation matrix.
 *
 */

#ifndef MODEL_HPP_
#define MODEL_HPP_

#define _USE_MATH_DEFINES

#include <cmath>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "OffParser.hpp"
#include "ProgramObject.hpp"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

class Model
{
public:
	~Model();
	static Model* CreateFromFile(string filename, ProgramObject* po);
	void Translate(vec2 translation);
	void Translate(vec3 translation);
	void RotateX(float angle);
	void RotateY(float angle);
	void RotateZ(float angle);
	void Scale(float scale_factor);
	mat4 GetModelMatrix();
	void Draw();
	void SetModelLocation(GLuint model_location);
	void InitializeBuffers();
	void SetProgramObject(ProgramObject* program_object);

private:
	Model();
	mat4 identity = mat4(1.0f);
	vec3 x_axis = vec3(1.0f, 0.0f, 0.0f);
	vec3 y_axis = vec3(0.0f, 1.0f, 0.0f);
	vec3 z_axis = vec3(0.0f, 0.0f, 1.0f);

	mat4 model_matrix = mat4(1.0f);
	mat4 translation_matrix = mat4(1.0f);
	mat4 rotation_matrix = mat4(1.0f);
	mat4 scale_matrix = mat4(1.0f);

	mat4 rotation_matrix_x = mat4(1.0f);
	mat4 rotation_matrix_y = mat4(1.0f);
	mat4 rotation_matrix_z = mat4(1.0f);
	float rotation_x = 0.0f;
	float rotation_y = 0.0f;
	float rotation_z = 0.0f;
	float scale_factor = 1.0f;
	vec2 translation = vec2(0.0f);

	Object* object = nullptr;
	GLuint model_location = 0;
	GLuint buffers[2];
	GLuint vao;
	GLuint position_location;
	GLuint normal_location;
	ProgramObject* program_object;
};

#endif


