/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  PrintMatrix.hpp
 *
 *  Used for debugging.
 *
 */

#ifndef PRINT_MATRIX
#define PRINT_MATRIX

#include <iostream>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

void PrintMatrix(glm::mat4 matrix, std::string text);
void PrintVector(glm::vec3 v, std::string s);

#endif
