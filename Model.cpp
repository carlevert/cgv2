/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Model.cpp
 *
 *  Model-objects have their own vertex array and buffers, and of course a
 *  model transformation matrix.
 *
 */

#include "Model.hpp"
#include "PrintMatrix.hpp"

Model::Model()
{
}

Model::~Model()
{
	glDeleteBuffers(2, buffers);
	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);
}

void Model::InitializeBuffers()
{
	unsigned int num_vertices = object->GetNumVertices();
	unsigned int vertices_bytes = num_vertices * sizeof(float) * 3;
	unsigned int normal_bytes = num_vertices * sizeof(float) * 3;
	unsigned int color_bytes = num_vertices * sizeof(char) * 4;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(2, buffers);
	glBindBuffer(GL_ARRAY_BUFFER, buffers[0]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[1]);

	position_location = program_object->GetAttribLocation("position");
	normal_location = program_object->GetAttribLocation("normal");

	// Initialize buffer for model
	glBufferData(GL_ARRAY_BUFFER, vertices_bytes + normal_bytes, NULL, GL_STATIC_DRAW);

	// Buffer vertex positions and normals
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices_bytes, object->GetVertices());
	glBufferSubData(GL_ARRAY_BUFFER, vertices_bytes, normal_bytes,
			object->GetNormals());


	glVertexAttribPointer(position_location, 3, GL_FLOAT, GL_FALSE, 0,
			BUFFER_OFFSET(0));
	glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE, 0,
			BUFFER_OFFSET(vertices_bytes));

	glEnableVertexAttribArray(position_location);
	glEnableVertexAttribArray(normal_location);

	// Get indicies for triangles ..
	GLuint* indicies = new GLuint[object->GetNumFaces() * 3];
	for (int i = 0; i < object->GetNumFaces(); i++) {
		indicies[i * 3 + 0] = object->GetFace(i)->GetVertexIndicies()[0];
		indicies[i * 3 + 1] = object->GetFace(i)->GetVertexIndicies()[1];
		indicies[i * 3 + 2] = object->GetFace(i)->GetVertexIndicies()[2];
	}
	// ... and copy them to the ELEMENT_ARRAY_BUFFER
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			sizeof(GLuint) * 3 * object->GetNumFaces(), indicies,
			GL_DYNAMIC_DRAW);
	delete[] indicies;

	// Unbind vertex array object so it doesn't get written to by mistake
	glBindVertexArray(0);
}

void Model::Draw()
{
	if (object == nullptr)
		return;
	const float* model_matrix = glm::value_ptr(GetModelMatrix());
	glUniformMatrix4fv(model_location, 1, GL_FALSE, model_matrix);
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, object->GetNumFaces() * 3, GL_UNSIGNED_INT,
			BUFFER_OFFSET(0));
	glBindVertexArray(0);

}

void Model::SetProgramObject(ProgramObject* program_object)
{
	this->program_object = program_object;
	model_location = program_object->GetUniformLocation("model");
	InitializeBuffers();
}

Model* Model::CreateFromFile(string filename, ProgramObject* program_object)
{
	Object* object = OffParser::ParseOffFile(filename);
	if (object != nullptr) {
		object->CalculateNormals();
		object->Normalize();
		object->Triangulate();
		Model* new_model = new Model();
		new_model->object = object;
		new_model->SetProgramObject(program_object);
		return new_model;
	} else {
		cout << "Model::CreateFromFile: object null" << endl;
		exit(1);
	}
}

void Model::Translate(glm::vec2 translation)
{
	this->translation += translation;
	translation_matrix = glm::translate(glm::mat4(1.0f),
			glm::vec3(this->translation, 0.0f));
}

void Model::Translate(glm::vec3 translation)
{
	this->translation += vec2(translation);
	translation_matrix = glm::translate(glm::mat4(1.0f), vec3(this->translation, translation.z));
}

void Model::RotateX(float angle)
{
	angle = angle / 180.0f * M_PI;
	rotation_x += angle;
	rotation_matrix_x = glm::rotate(identity, rotation_x, x_axis);
}

void Model::RotateY(float angle)
{
	angle = angle / 180.0f * M_PI;
	rotation_y += angle;
	rotation_matrix_y = glm::rotate(identity, rotation_y, y_axis);
}

void Model::RotateZ(float angle)
{
	angle = angle / 180.0f * M_PI;
	rotation_z += angle;
	rotation_matrix_z = glm::rotate(identity, rotation_z, z_axis);
}

void Model::Scale(float scale_factor)
{
	this->scale_factor *= scale_factor;
	scale_matrix = glm::scale(identity, glm::vec3(this->scale_factor));
}

glm::mat4 Model::GetModelMatrix()
{
	rotation_matrix = rotation_matrix_x * rotation_matrix_y * rotation_matrix_z;
	model_matrix = translation_matrix * rotation_matrix * scale_matrix;
	return model_matrix;
}

void Model::SetModelLocation(GLuint model_location)
{
	this->model_location = model_location;
}
