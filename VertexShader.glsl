/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  VertexShader.glsl
 *
 *  Final vertex-shader.
 *  Calculates glPosition. world_position and world_normal are sent to the
 *  fragment shader. Textures are sphere-mapped and corresponding U & V
 *  coordinates are calculated. 
 *
 */
 
 #version 330 core
#define M_PI 3.1415926535897932384626433832795

in vec3 position;
in vec3 normal;

out vec2 UV;

smooth out vec3 world_normal;
smooth out vec3 world_position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float flip_normals;

void main() {

	gl_Position = projection * view * model * vec4(position, 1.0);

	world_position = vec3(model * vec4(position, 1.0f));
	world_normal = vec3(model * vec4(normal, 0.0f)) * flip_normals;

	// Calculate u & v coordinates
	float vv = acos(
		clamp(dot(
			normalize(position),
			normalize(vec3(0.0, 0.0, 1.0))
		), -1.0, 1.0));

	vec2 xy = position.xy;

	// Prevent division by zero at north and south pole.
	if (abs(xy.x) > 0.001 || abs(xy.y) > 0.001)
		xy = normalize(xy);
	else
		xy = vec2(0.0, 0.0);

	float u = acos(clamp(dot(xy, vec2(1.0, 0.0)), -1.0, 1.0));

	UV = vec2(u / M_PI, vv / M_PI);


}

