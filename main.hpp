/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  main.hpp
 *
 *  Contains main-function. Creates an OpenGL context with GLFW.
 *
 */

#ifndef MAIN_HPP_
#define MAIN_HPP_

#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <gtk/gtk.h>
extern "C" {
#include "guicontrol.h"
#include "guicallback.h"
}

#include "Application.hpp"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

Application* application;
void* app;
float rotation_scale = 0.002f;
bool rotating_camera_x_y = false;
double start_x, start_y, end_x, end_y;
bool running = true;


int main(int argc, char** argv);
GLFWwindow* SetupGlfw();
void GlfwErrorCallback(int error, const char* description);
void GlfwWindowCloseCallback(GLFWwindow* window);
void GlfwFramebufferSizeCallback(GLFWwindow* window, int width, int height);
void GlfwMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void GlfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void GlfwScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
void SetGuiDefaults(void);

#endif
