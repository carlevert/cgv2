/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Application.cpp
 *
 *  The Application class is responsible for a lot of things. It contains a
 *  vector of Model-objects, objects modeling light, a material object (holding
 *  material properties) as well as a Camera-object and a Projection-object.
 *
 *  The OpenGL context is set up in the constructor and there is a method
 *  GetUniformLocations for getting the uniform locations for the current
 *  program object.
 *
 *  Some notable comments regarding the design. There is a number of boolean
 *  member variables named invalid_*, like for example invalid_light_properties.
 *  When true, the corresponding  uniform(s) are updated and the variable is
 *  reset to false. The check is done in the Display function and the variables
 *  are set from their corresponding object.
 *
 *  Several models can be loaded. The models are stored in a vector from index
 *  1 and up. Model at index 0 is the ground floor. Model transformations are
 *  applied to the currently selected object. The ground floor can not be
 *  selected.
 *
 */

#include "Application.hpp"

Application::Application()
{
	// Set graphics attributes
	glLineWidth(1.0);
	glPointSize(1.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glViewport(0, 0, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
	glEnable(GL_MULTISAMPLE);

	CreateOpenGlProgram();

	// These references are used to trigger an update of the corresponding
	// uniform(s)
	camera.SetInvalidViewMatrixRef(&invalid_view_matrix);
	projection.SetInvalidProjectionMatrixRef(&invalid_projection_matrix);
	light.SetInvalidLightPosRef(&invalid_light_properties);
	material.SetInvalidMaterialRef(&invalid_material_properties);
	ambient.SetInvalidateAmbientLightRef(&invalid_ambient_light_properties);

	// Generates texture names
	glGenTextures(1, textures);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Ground floor
	OpenOffFile("off/plane.off");
	selected_model_no = 0;
	models.at(0)->Scale(3);
	models.at(0)->Translate(vec3(0.0f, 0.0f, -0.5f));
}

Application::~Application()
{
	glDeleteTextures(1, textures);
}

/*
 * Gets uniform locations from OpenGL program.
 */
void Application::GetUniformLocations()
{
	// Matrices
	model_location = program_object->GetUniformLocation("model");
	view_location = program_object->GetUniformLocation("view");
	projection_location = program_object->GetUniformLocation("projection");

	// Light
	light_pos_location = program_object->GetUniformLocation("light_pos");
	light_intensity_location = program_object->GetUniformLocation(
			"light_intensity");
	ambient_light_intensity_location = program_object->GetUniformLocation(
			"ambient_light_intensity");

	// Ambient light locations
	ambient_coeff_location = program_object->GetUniformLocation("ambient");
	diffuse_coeff_location = program_object->GetUniformLocation("diffuse");
	specular_coeff_location = program_object->GetUniformLocation("specular");

	// Misc uniforms
	phong_location = program_object->GetUniformLocation("phong");
	flip_normals_location = program_object->GetUniformLocation("flip_normals");
	show_texture_location = program_object->GetUniformLocation("show_texture");

	InvalidateUniforms();
}
/*
 * Setting all invalid_* variables true will cause all uniforms to be updated
 * next time Display() is called
 */
void Application::InvalidateUniforms()
{
	invalid_view_matrix = true;
	invalid_projection_matrix = true;
	invalid_light_properties = true;
	invalid_ambient_light_properties = true;
	invalid_material_properties = true;
	invalid_flip_normals = true;
	invalid_show_texture = true;
}

/*
 * Creates an OpenGL program from two files. Can be called several times.
 */
void Application::CreateOpenGlProgram()
{
	ProgramObject* new_program_object = ProgramObject::CreateFromFiles(
			"VertexShader.glsl", "FragmentShader.glsl");
	if (new_program_object == nullptr)
		return;
	if (program_object != nullptr)
		delete program_object;
	program_object = new_program_object;
	program_object->Use();
	GetUniformLocations();
}

void Application::OpenOffFile(string filename)
{
	models.push_back(Model::CreateFromFile(filename, program_object));
	selected_model_no = models.size() - 1;
}

void Application::Display()
{
	// If shader_source_changed is set the current OpenGL
	// program will be replaced.
	if (shader_source_changed) {
		// delete program_object;
		CreateOpenGlProgram();
		for (Model* model : models) {
			model->SetProgramObject(program_object);
			model->InitializeBuffers();
		}
		shader_source_changed = false;
	}

	// Update uniforms if necessary
	if (invalid_view_matrix)
		UpdateViewMatrix();
	if (invalid_projection_matrix)
		UpdateProjectionMatrix();
	if (invalid_light_properties)
		UpdateLightProperties();
	if (invalid_ambient_light_properties)
		UpdateAmbientLightProperties();
	if (invalid_material_properties)
		UpdateMaterialProperties();
	if (invalid_flip_normals)
		UpdateFlipNormals();
	if (invalid_show_texture)
		UpdateShowTexture();

	// Load texture
	if (texture_filename != nullptr) {
		int texture_width, texture_height, texture_channels;
		unsigned char* data;
		data = SOIL_load_image(texture_filename, &texture_width, &texture_height,
				&texture_channels, SOIL_LOAD_AUTO);
		glBindTexture(GL_TEXTURE_2D, textures[0]);
		if (data != 0)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture_width, texture_height,
					0, GL_RGB,
					GL_UNSIGNED_BYTE, data);
		SOIL_free_image_data(data);
		SetShowTexture(true);
		texture_filename = nullptr;
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, textures[1]);
	models.at(0)->Draw();

	glBindTexture(GL_TEXTURE_2D, show_texture ? textures[0] : 0);

	for (int i = 1; i < models.size(); i++)
		models[i]->Draw();
	glFlush();
}

/*
 * Sets member variable flip_normals and triggers an upload of the flip-normals-
 * uniform..
 */
void Application::SetFlipNormals(bool flip_normals)
{
	this->flip_normals = flip_normals;
	invalid_flip_normals = true;
}

/*
 * Uploads a new view matrix.
 */
void Application::UpdateViewMatrix()
{
	const float* view_matrix = glm::value_ptr(camera.GetViewMatrix());
	glUniformMatrix4fv(view_location, 1, GL_FALSE, view_matrix);
	invalid_view_matrix = false;
}

/*
 * Uploads a new projection matrix.
 */
void Application::UpdateProjectionMatrix()
{
	const float* projection_matrix = glm::value_ptr(
			projection.GetProjectionMatrix());
	glUniformMatrix4fv(projection_location, 1, GL_FALSE, projection_matrix);
	invalid_projection_matrix = false;
}

/*
 * Upload new light properties (intensity and position).
 */
void Application::UpdateLightProperties()
{
	const float* intensity = light.GetIntensity();
	const float* position = light.GetPosition();
	glUniform3fv(light_intensity_location, 1, intensity);
	glUniform3fv(light_pos_location, 1, position);
	invalid_light_properties = false;
}

/*
 * Uploads new material properties.
 */
void Application::UpdateMaterialProperties()
{
	glUniform3fv(ambient_coeff_location, 1, material.GetAmbient());
	glUniform3fv(diffuse_coeff_location, 1, material.GetDiffuse());
	glUniform3fv(specular_coeff_location, 1, material.GetSpecular());
	glUniform1i(phong_location, material.GetPhong());
	invalid_material_properties = false;
}

/*
 * Upload new ambient light properties.
 */
void Application::UpdateAmbientLightProperties()
{
	glUniform3fv(ambient_light_intensity_location, 1, ambient.GetIntensity());
	invalid_ambient_light_properties = false;
}

/*
 * Setter for viewport dimensions. Also calls glViewport with new dimensions.
 */
void Application::SetViewportSize(int width, int height)
{
	projection.SetViewportDimensions(width, height);
	glViewport(0, 0, width, height);
}

/*
 * Uploads a float for flipping normals.
 * (Some person on The internets suggested using floats by some reason I
 * don't remember).
 */
void Application::UpdateFlipNormals()
{
	if (flip_normals)
		glUniform1f(flip_normals_location, 1.0f);
	else
		glUniform1f(flip_normals_location, -1.0f);
}

/*
 * Sets member variable texture_filename. When set, next call to Display()
 * will try to read that file and set texture_filename to nullptr.
 */
void Application::SetTextureFilename(char* texture_filename)
{
	this->texture_filename = texture_filename;
}

void Application::SetShowTexture(bool show_texture)
{
	this->show_texture = show_texture;
	invalid_show_texture = true;
}

void Application::UpdateShowTexture()
{
	glUniform1f(show_texture_location, show_texture ? 1.0f : 0.0f);
	invalid_show_texture = false;
}

/*
 * Selects which model to apply transformations to.
 */
void Application::SelectModel(unsigned int model_no)
{
	if (model_no > 0 && model_no < models.size()) {
		cout << "Model no " << model_no << " selected." << endl;
		selected_model_no = model_no;
	}
}

/*
 *  Returns currently selected model.
 */
Model* Application::GetModel()
{
	return models.at(selected_model_no);
}

Camera* Application::GetCamera()
{
	return &camera;
}

Projection* Application::GetProjection()
{
	return &projection;
}

Light* Application::GetLight()
{
	return &light;
}

Object* Application::GetObject()
{
	return current_object;
}

Material* Application::GetMaterial()
{
	return &material;
}

Ambient* Application::GetAmbient()
{
	return &ambient;
}
