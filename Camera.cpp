/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Camera.cpp
 *
 *  Camera can be translated in camera space and rotated aorund U or V. Also
 *  responsible for creating the view-matrix.
 *
 */

#include "Camera.hpp"

Camera::Camera()
{
	Translate(initial_position);
}

Camera::~Camera()
{
}

/*
 * Translate camera relative to its current position.
 */
void Camera::Translate(vec3 translation)
{
	mat4 view = GetViewMatrix();
	vec4 translation_wc = transpose(view) * vec4(-translation, 0);
	translation_matrix = translate(translation_matrix, vec3(translation_wc));
	InvalidateViewMatrix();
}

/*
 * Returns view matrix.
 */
mat4 Camera::GetViewMatrix()
{
	mat4 temp_rotation_matrix = rotation_matrix_u * rotation_matrix_v
			* rotation_matrix;
	return temp_rotation_matrix * translation_matrix;
}

/*
 * Called to commit a rotation. When rotating camera by mouse, the rotation
 * around U and V are saved elsewhere until rotation stops.
 */
void Camera::Commit()
{
	mat4 temp_rotation_matrix = rotation_matrix_u * rotation_matrix_v
			* rotation_matrix;
	rotation_matrix = temp_rotation_matrix;
	InvalidateViewMatrix();
	rotation_matrix_u = identity;
	rotation_matrix_v = identity;
}

/*
 * Rotates camera around U.
 */
void Camera::RotateU(float angle)
{
	rotation_matrix_u = rotate(identity, angle, vec3(1, 0, 0));
	InvalidateViewMatrix();
}

/*
 * Rotates camera around V.
 */
void Camera::RotateV(float angle)
{
	rotation_matrix_v = rotate(identity, angle, vec3(0, 1, 0));
	InvalidateViewMatrix();
}

void Camera::InvalidateViewMatrix()
{
	if (invalid_view_matrix != nullptr)
		*invalid_view_matrix = true;
}

void Camera::SetInvalidViewMatrixRef(bool* invalid_view_matrix)
{
	this->invalid_view_matrix = invalid_view_matrix;
}
