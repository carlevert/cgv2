/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Face.hpp
 *
 *  Represents a face in Object-objects.
 *
 */

#ifndef FACE_HPP
#define FACE_HPP

#include <iostream>

class Face {
public:
	Face();
	~Face();
	void SetNumVertices(unsigned int num_vertices);
	unsigned int GetNumVertices() const;
	void SetVertexIndicies(unsigned int* vertices);
	unsigned int* GetVertexIndicies() const;
	void Print() const;
	Face* RemoveEar();
	float* GetNormal() const;

private:
	unsigned int num_vertex_indicies = 0;
	unsigned int* vertex_indicies = nullptr;
};

#endif
