/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Application.hpp
 *
 *  The Application class is responsible for a lot of things. It contains a
 *  vector of Model-objects, objects modeling light, a material object (holding
 *  material properties) as well as a Camera-object and a Projection-object.
 *
 *  The OpenGL context is set up in the constructor and there is a method
 *  GetUniformLocations for getting the uniform locations for the current
 *  program object.
 *
 *  Some notable comments regarding the design. There is a number of boolean
 *  member variables named invalid_*, like for example invalid_light_properties.
 *  When true, the corresponding  uniform(s) are updated and the variable is
 *  reset to false. The check is done in the Display function and the variables
 *  are set from their corresponding object.
 *
 *  Several models can be loaded. The models are stored in a vector from index
 *  1 and up. Model at index 0 is the ground floor. Model transformations are
 *  applied to the currently selected object. The ground floor can not be
 *  selected.
 *
 */

#ifndef APPLICATION_HPP_
#define APPLICATION_HPP_

#include <cmath>
#include <iomanip>
#include <vector>
#include <iostream>
#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "SOIL/SOIL.h"

#include "ProgramObject.hpp"
#include "Object.hpp"
#include "OffParser.hpp"
#include "Projection.hpp"
#include "Camera.hpp"
#include "Model.hpp"
#include "Light.hpp"
#include "Material.hpp"
#include "Ambient.hpp"

using namespace glm;
using namespace std;

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

class Application {
private:
	// OpenGL stuff

	GLuint vao = 0;
	GLuint buffers[2];
	GLuint textures[2];

	GLuint position_location;
	GLuint color_location;
	GLuint normal_location;

	GLuint model_location;
	GLuint view_location;
	GLuint projection_location;

	GLuint light_pos_location;
	GLuint light_intensity_location;
	GLuint ambient_light_intensity_location;

	GLuint ambient_coeff_location;
	GLuint diffuse_coeff_location;
	GLuint specular_coeff_location;

	GLuint phong_location;
	GLuint flip_normals_location;
	GLuint show_texture_location;

	ProgramObject* program_object = nullptr;

	Object* current_object = nullptr;

	Camera camera;
	Projection projection;

	vector<Model*> models;

	Light light;
	Material material;
	Ambient ambient;

	bool invalid_view_matrix = true;
	bool invalid_model_matrix = true;
	bool invalid_projection_matrix = true;
	bool invalid_light_properties = true;
	bool invalid_material_properties = true;
	bool invalid_ambient_light_properties = true;
	bool invalid_flip_normals = true;
	bool invalid_show_texture = true;

	void UpdateModelMatrix();
	void UpdateProjectionMatrix();
	void UpdateViewMatrix();
	void UpdateLightProperties();
	void UpdateAmbientLightProperties();
	void UpdateMaterialProperties();
	void UpdateFlipNormals();
	void UpdateShowTexture();


	void CreateOpenGlProgram();
	void GetUniformLocations();
	void InitializeBuffers();
	void InvalidateUniforms();
	bool flip_normals = false;
	char* texture_filename = nullptr;
	char* loaded_texture_filename = nullptr;
	int selected_model_no = 0;

public:
	char* off_to_load = nullptr;
	bool shader_source_changed = false;
	bool show_texture = false;
	bool texture_bound = false;

	static const unsigned int INITIAL_WINDOW_WIDTH = 640;
	static const unsigned int INITIAL_WINDOW_HEIGHT = 480;
	Application();
	~Application();
	void Display();
	void OpenOffFile(string filename);
	void SetViewportSize(int width, int height);
	void SetFlipNormals(bool flip_normals);
	void SetTextureFilename(char* texture_filename);
	void SetShowTexture(bool show_texture);
	void SelectModel(unsigned int model_no);
	unsigned int GetSelectedModelNo() { return selected_model_no; }
	Model* GetModel();

	Camera* GetCamera();
	Projection* GetProjection();
	Light* GetLight();
	Object* GetObject();
	Material* GetMaterial();
	Ambient* GetAmbient();
};

#endif
