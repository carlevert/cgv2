/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  AppWrapp.hpp
 *
 *  Adapter for making C++ object accessible from C.
 *
 */

#ifndef APPWRAP_HPP_
#define APPWRAP_HPP_

#include "Application.hpp"
#include "Projection.hpp"

#include <iostream>
#include <string>

extern "C" {

#include "guicontrol.h"

// Misc

void call_OpenOffFile(struct Application*, char* filename);
void call_FlipNormals(struct Application*, int flip_normals);

// Light

void call_Light_SetPositionX(struct Application*, float value);
void call_Light_SetPositionY(struct Application*, float value);
void call_Light_SetPositionZ(struct Application*, float value);

void call_Light_SetIntensityR(struct Application*, float value);
void call_Light_SetIntensityG(struct Application*, float value);
void call_Light_SetIntensityB(struct Application*, float value);

// Ambient light

void call_Ambient_SetIntensityR(struct Application*, float value);
void call_Ambient_SetIntensityG(struct Application*, float value);
void call_Ambient_SetIntensityB(struct Application*, float value);

// Material

void call_Material_SetAmbientCoefficientR(struct Application*, float value);
void call_Material_SetAmbientCoefficientG(struct Application*, float value);
void call_Material_SetAmbientCoefficientB(struct Application*, float value);

void call_Material_SetSpecularCoefficientR(struct Application*, float value);
void call_Material_SetSpecularCoefficientG(struct Application*, float value);
void call_Material_SetSpecularCoefficientB(struct Application*, float value);

void call_Material_SetDiffuseCoefficientR(struct Application*, float value);
void call_Material_SetDiffuseCoefficientG(struct Application*, float value);
void call_Material_SetDiffuseCoefficientB(struct Application*, float value);

void call_SetPhongValue(struct Application*, int value);

// Perspective & projection

void call_SetFov(struct Application*, float fov);
void call_SetParallellProjection(struct Application*);
void call_SetPerspectiveProjection(struct Application*);
void call_SetTop(struct Application*, float top);
void call_SetFar(struct Application*, float far);
void call_SetObliqueScale(struct Application*, float oblique_scale);
void call_SetObliqueAngle(struct Application*, float oblique_angle);

void call_SetTextureFilename(struct Application*, char* filename);
void call_ToggleTexture(struct Application*, int show_texture);

} // extern "C"

#endif /* APPWRAP_HPP_ */
