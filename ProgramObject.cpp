/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  ProgramObject.cpp
 *
 *  Represents an OpenGL program.
 *
 */

#include "ProgramObject.hpp"

ProgramObject::ProgramObject() {
	program_id = glCreateProgram();
}

void ProgramObject::AttachShader(Shader* shader) {
	glAttachShader(program_id, shader->GetId());
}

ProgramObject::~ProgramObject() {
	glDeleteProgram(program_id);
}

void ProgramObject::LinkProgram() {
	glLinkProgram(program_id);
}

bool ProgramObject::LinkedSuccessfully() {
	GLint linked_successfully;
	glGetProgramiv(program_id, GL_LINK_STATUS, &linked_successfully);
	return linked_successfully == GL_TRUE;
}

string ProgramObject::GetLinkingErrorMessage() {
	GLint log_length;
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_length);
	GLchar* info_log = new GLchar[log_length];
	glGetProgramInfoLog(program_id, log_length, NULL, info_log);
	string error_message(info_log);
	delete[] info_log;
	return error_message;

}

GLuint ProgramObject::GetAttribLocation(const GLchar* name) {
	return glGetAttribLocation(program_id, name);
}

GLuint ProgramObject::GetUniformLocation(const GLchar* name) {
	return glGetUniformLocation(program_id, name);
}

void ProgramObject::Use() {
	glUseProgram(program_id);
}

GLuint ProgramObject::GetProgramId() {
	return program_id;
}

ProgramObject* ProgramObject::CreateFromFiles(string vertex_shader_filename,
		string fragment_shader_filename) {

	Shader* vertex_shader = VertexShader::CreateFromFile(vertex_shader_filename);
	Shader* fragment_shader = FragmentShader::CreateFromFile(fragment_shader_filename);

	ProgramObject* program_object = new ProgramObject();

	program_object->AttachShader(vertex_shader);
	program_object->AttachShader(fragment_shader);

	program_object->LinkProgram();
	if (!program_object->LinkedSuccessfully()) {
		cerr << "Linking error" << endl;
		cerr << program_object->GetLinkingErrorMessage() << endl;
		return nullptr;
	}

	return program_object;
}
