/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Material.hpp
 *
 *  Material-objects stores information about material properties.
 *
 */

#ifndef MATERIAL_HPP_
#define MATERIAL_HPP_

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;

class Material {
public:
	Material();
	virtual ~Material();

	void SetAmbientR(float value);
	void SetAmbientG(float value);
	void SetAmbientB(float value);

	void SetDiffuseR(float value);
	void SetDiffuseG(float value);
	void SetDiffuseB(float value);

	void SetSpecularR(float value);
	void SetSpecularG(float value);
	void SetSpecularB(float value);

	void SetPhong(unsigned int value);

	float* GetAmbient();
	float* GetDiffuse();
	float* GetSpecular();
	unsigned int GetPhong();

	void SetInvalidMaterialRef(bool* invalid_material);

private:
	bool* invalid_material = nullptr;
	void InvalidateMaterial();
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	unsigned int phong = 1;
};

#endif
