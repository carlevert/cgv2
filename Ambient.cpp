/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Ambient.cpp
 *
 *  Class Ambient is holding the ambient light properties.
 *
 */

#include "Ambient.hpp"

Ambient::Ambient()
{
}

Ambient::~Ambient()
{
}
/*
 * Getter-method for ambient light intensity. Returns intensity as
 *  pointer to floats.
 */
float* Ambient::GetIntensity()
{
	return value_ptr(intensity);
}

/*
 * Setter for red component of ambient light intensity.
 */
void Ambient::SetIntensityR(float intensity)
{
	this->intensity.r = intensity;
	InvalidateAmbientLight();
}

/*
 * Setter for green component of ambient light intensity.
 */
void Ambient::SetIntensityG(float intensity)
{
	this->intensity.g = intensity;
	InvalidateAmbientLight();
}

/*
 * Setter for blue component of ambient light intensity.
 */
void Ambient::SetIntensityB(float intensity)
{
	this->intensity.b = intensity;
	InvalidateAmbientLight();
}

/*
 * Flag changes in ambient light.
 */
void Ambient::InvalidateAmbientLight()
{
	if (invalid_ambient_light_properties != nullptr)
		*invalid_ambient_light_properties = true;
}

/*
 * Sets pointer to a boolean variable which indicates change in ambient light.
 */
void Ambient::SetInvalidateAmbientLightRef(
		bool* invalid_ambient_light_properties)
{
	this->invalid_ambient_light_properties = invalid_ambient_light_properties;
}
