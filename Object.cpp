/*
 *  Computer Graphics and Visualization 5DV111
 *
 *  Assignment 3 - 3dstudio, Walking in the air
 *
 *  2016-01-14
 *
 *  Carl-Evert Kangas
 *  dv14cks@cs.umu.se
 *
 *  Object.cpp
 *
 *  Contains vertex and normals data. Contains a method that can calculate
 *  normals from faces and a method that squeezes the model in a unit cube.
 *
 */

#include "Object.hpp"
#include "PrintMatrix.hpp"

Object::Object()
{
}

Object::~Object()
{
	if (vertices != nullptr)
		delete vertices;
	if (normals != nullptr)
		delete normals;
}

unsigned int Object::GetNumVertices()
{
	return num_vertices;
}

void Object::SetNumVertices(unsigned int num_vertices)
{
	if (vertices != nullptr)
		delete vertices;
	if (normals != nullptr)
		delete normals;
	this->num_vertices = num_vertices;
	vertices = new vec3[num_vertices];
	normals = new vec3[num_vertices];

}

/*
 * Returns pointer to vertex coordinates.
 */
float* Object::GetVertices()
{
	return value_ptr(vertices[0]);
}

/*
 * Returns pointer to vertex normals.
 */
float* Object::GetNormals()
{
	return value_ptr(normals[0]);
}

/*
 * Returns current number of faces in object.
 */
unsigned int Object::GetNumFaces()
{
	return faces.size();
}

///void Object::SetNumFaces(int num_faces)
//{
//}

//int Object::GetNumEdges()
//{
//	return this->num_edges;
//}

//void Object::SetNumEdges(int num_edges)
//{
//	this->num_edges = num_edges;
//}

/*
 * Adds sets coordinates for a vertex in the vec3 array of vertices.
 */
void Object::AddVertex(float* vertex)
{
	vertices[vertex_cursor].x = vertex[0];
	vertices[vertex_cursor].y = vertex[1];
	vertices[vertex_cursor].z = vertex[2];
	vertex_cursor++;
}

/*
 * Adds a pointer to Face to vector containing faces.
 */
void Object::AddFace(Face* face)
{
	faces.push_back(face);
	num_faces++;
}

/*
 * Returns pointer to Face at position i.
 */
Face* Object::GetFace(unsigned int i)
{
	return faces.at(i);
}

/*
 * Calculates normals for the object by constructing two vectors from the three
 * first vertices and then taking the cross product of those.
 * The normals are normalized.
 */
void Object::CalculateNormals()
{
	for (Face* face : faces) {

		unsigned int* vertex_indicies = face->GetVertexIndicies();

		// Construct two vectors v1 and v2 from three first vertices.
		vec3 v1 = vertices[vertex_indicies[1]] - vertices[vertex_indicies[0]];
		vec3 v2 = vertices[vertex_indicies[2]] - vertices[vertex_indicies[0]];

		// Add current normal to previously calculated normals for the face.
		vec3 normal = normalize(cross(v1, v2));
		for (int i = 0; i < face->GetNumVertices(); i++)
			normals[vertex_indicies[i]] += normal;

	}

	// Finally normalize all normals.
	for (int i = 0; i < GetNumVertices(); i++)
		normals[i] = normalize(normals[i]);
}

/*
 * Makes object fit in a 1x1x unit-cube. Also translates object center
 *  to the origin.
 */
void Object::Normalize()
{
	// Start with min and max coordinates set to coordinates of first vertex
	float min_x, max_x;
	float min_y, max_y;
	float min_z, max_z;
	min_x = max_x = vertices[0].x;
	min_y = max_y = vertices[0].y;
	min_z = max_z = vertices[0].z;

	// Get min and max coordinates
	vec3* vertex;
	for (int i = 1; i < num_vertices; i++) {
		vertex = &vertices[i];

		if (vertex->x < min_x)
			min_x = vertex->x;
		else if (vertex->x > max_x)
			max_x = vertex->x;

		if (vertex->y < min_y)
			min_y = vertex->y;
		else if (vertex->y > max_y)
			max_y = vertex->y;

		if (vertex->z < min_z)
			min_z = vertex->z;
		else if (vertex->z > max_z)
			max_z = vertex->z;
	}

	// Find the largest extension along x, y or z
	float size_x = max_x - min_x;
	float size_y = max_y - min_y;
	float size_z = max_z - min_z;

	float size_max = size_x;
	if (size_y > size_max)
		size_max = size_y;
	if (size_z > size_max)
		size_max = size_z;

	// Scale and center around at origin
	for (int i = 0; i < num_vertices; i++) {
		vertex = &vertices[i];
		vertex->x = ((vertex->x - min_x) - size_x / 2) / size_max;
		vertex->y = ((vertex->y - min_y) - size_y / 2) / size_max;
		vertex->z = ((vertex->z - min_z) - size_z / 2) / size_max;
	}



}

/*
 * Converts objects faces to triangles by removing one ear at a time
 * while face have more than three vertices.
 * This approach only works with convex faces.
 */
void Object::Triangulate()
{
	int num_faces_start = num_faces;
	for (int i = 0; i < num_faces_start; i++) {
		Face* face = faces[i];
		while (face->GetNumVertices() > 3) {
			Face* removed_ear = face->RemoveEar();
			AddFace(removed_ear);
		}
	}

}

void Object::Print()
{
	cout << "---------- OBJECT ----------" << endl;
	cout << "NUM_VERTICES, NUM_FACES, NUM_EDGES:" << endl;
	cout << num_vertices << " " << num_faces << " " << num_edges << endl;
	cout << "VERTICES:" << endl;
	for (int i = 0; i < num_vertices; i++) {
		cout << fixed << setw(4) << i + 1 << ": ";
		cout << fixed << setw(7) << setprecision(4) << vertices[i].x;
		cout << fixed << setw(7) << setprecision(4) << vertices[i].y;
		cout << fixed << setw(7) << setprecision(4) << vertices[i].z << endl;
	}
	cout << "FACES:" << endl;
	for (int i = 0; i < faces.size(); i++) {
		cout << fixed << setw(4) << i + 1 << ": ";
		faces.at(i)->Print();
	}
	cout << "NORMALS:" << endl;
	for (int i = 0; i < GetNumVertices(); i++) {
		PrintVector(normals[i], "");
		cout << endl;
	}

}
